{#
    Unique filter for quickbook entities.
    Keep the last updated version of the entity.
#}
{% macro unique_filter(table_name) -%}

    id in (
        select id
        from {{ table_name }}
        group by id
        having _airbyte_emitted_at = max(_airbyte_emitted_at)
    )

{%- endmacro %}


{#
    Unique select for quickbook entities.
    Keep the last updated version of the entity.
#}
{% macro unique_select(table_name) -%}

    select * from {{ table_name }}
    where {{ unique_filter(table_name) }}

{%- endmacro %}