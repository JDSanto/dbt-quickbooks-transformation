{#
    Common fields for all QuickBooks entities
#}
{% macro common_fields() -%}
    {{ json_extract_scalar('_airbyte_data', ['Id']) }} as {{ adapter.quote('id') }},
    {{ string_to_timestamp(json_extract_scalar('_airbyte_data', ['MetaData', 'CreateTime'])) }} as {{ adapter.quote('created_at') }},
    {{ string_to_timestamp(json_extract_scalar('_airbyte_data', ['MetaData', 'LastUpdatedTime'])) }} as {{ adapter.quote('updated_at') }},
    _airbyte_emitted_at
{%- endmacro %}
