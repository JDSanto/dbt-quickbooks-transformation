{#
    Incremental filter for quickbook entities.
#}
{% macro incremental_filter() -%}

{% if is_incremental() %}
    _airbyte_emitted_at > (select max(_airbyte_emitted_at) from {{ this }})
{% else %}
    true
{% endif %}

{%- endmacro %}

{#
    Incremental filter for quickbook lines.
    It needs to be a different filter since it picks up the '_airbyte_emitted_at' 
    from the parent entity, which was already filtered.
#}
{% macro incremental_filter_line(parent_table) -%}

{% if is_incremental() %}
    _airbyte_emitted_at >= (select max(_airbyte_emitted_at) from {{ parent_table }})
{% else %}
    true
{% endif %}

{%- endmacro %}