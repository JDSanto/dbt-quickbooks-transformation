{#
    Generate a unique hash for a quickbook object row
#}
{% macro hash_field() -%}
    {{ dbt_utils.surrogate_key([
        adapter.quote('id'),
        adapter.quote('_airbyte_emitted_at')
    ]) }} as _hash_id
{%- endmacro %}
