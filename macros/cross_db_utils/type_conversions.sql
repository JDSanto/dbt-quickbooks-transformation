
{# boolean_to_string -------------------------------------------------     #}
{% macro boolean_to_string(boolean_column) -%}
  {{ adapter.dispatch('boolean_to_string')(boolean_column) }}
{%- endmacro %}

{% macro default__boolean_to_string(boolean_column) -%}
    {{ boolean_column }}
{%- endmacro %}

{# array_to_string -------------------------------------------------     #}
{% macro array_to_string(array_column) -%}
  {{ adapter.dispatch('array_to_string')(array_column) }}
{%- endmacro %}

{% macro default__array_to_string(array_column) -%}
    {{ array_column }}
{%- endmacro %}

{# cast_to_boolean -------------------------------------------------     #}
{% macro cast_to_boolean(field) -%}
    {{ adapter.dispatch('cast_to_boolean')(field) }}
{%- endmacro %}

{% macro default__cast_to_boolean(field) -%}
    cast({{ field }} as boolean)
{%- endmacro %}

{# -- MySQL does not support cast function converting string directly to boolean (an alias of tinyint(1), https://dev.mysql.com/doc/refman/8.0/en/cast-functions.html#function_cast #}
{% macro mysql__cast_to_boolean(field) -%}
    IF(lower({{ field }}) = 'true', true, false)
{%- endmacro %}

{# string_to_timestamp -------------------------------------------------     #}
{% macro string_to_timestamp(string_column) -%}
    STR_TO_DATE({{ string_column }}, '%Y-%m-%dT%H:%i:%s.%fZ')
{%- endmacro %}
