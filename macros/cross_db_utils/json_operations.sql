{#
    Adapter Macros for the following functions:
    - Bigquery: JSON_EXTRACT(json_string_expr, json_path_format) -> https://cloud.google.com/bigquery/docs/reference/standard-sql/json_functions
    - Snowflake: JSON_EXTRACT_PATH_TEXT( <column_identifier> , '<path_name>' ) -> https://docs.snowflake.com/en/sql-reference/functions/json_extract_path_text.html
    - Redshift: json_extract_path_text('json_string', 'path_elem' [,'path_elem'[, ...] ] [, null_if_invalid ] ) -> https://docs.aws.amazon.com/redshift/latest/dg/JSON_EXTRACT_PATH_TEXT.html
    - Postgres: json_extract_path_text(<from_json>, 'path' [, 'path' [, ...}}) -> https://www.postgresql.org/docs/12/functions-json.html
    - MySQL: JSON_EXTRACT(json_doc, 'path' [, 'path'] ...) -> https://dev.mysql.com/doc/refman/8.0/en/json-search-functions.html
#}

{# format_json_path --------------------------------------------------     #}
{% macro format_json_path(json_path_list) -%}
    {{ adapter.dispatch('format_json_path')(json_path_list) }}
{%- endmacro %}

{% macro default__format_json_path(json_path_list) -%}
    {{ '.' ~ json_path_list|join('.') }}
{%- endmacro %}

-- Modified to work with MariaDB
{% macro mysql__format_json_path(json_path_list) -%}
    {# -- '$."x"."y"."z"' #}
    {{ "'$.\"" ~ json_path_list|join("\".\"") ~ "\"'" }}
{%- endmacro %}

{# json_extract -------------------------------------------------     #}

{% macro json_extract(json_column, normalized_json_path) -%}
    {{ adapter.dispatch('json_extract')(json_column, normalized_json_path) }}
{%- endmacro %}

{% macro mysql__json_extract(json_column, normalized_json_path) -%}
    json_extract({{ json_column }}, {{ format_json_path(normalized_json_path) }})
{%- endmacro %}

{# json_extract_scalar -------------------------------------------------     #}

{% macro json_extract_scalar(json_column, normalized_json_path) -%}
    {{ adapter.dispatch('json_extract_scalar')(json_column, normalized_json_path) }}
{%- endmacro %}

{% macro mysql__json_extract_scalar(json_column, normalized_json_path) -%}
    json_value({{ json_column }}, {{ format_json_path(normalized_json_path) }})
{%- endmacro %}

{# json_extract_array -------------------------------------------------     #}

{% macro json_extract_array(json_column, normalized_json_path) -%}
    {{ adapter.dispatch('json_extract_array')(json_column, normalized_json_path) }}
{%- endmacro %}

{% macro mysql__json_extract_array(json_column, normalized_json_path) -%}
    json_extract({{ json_column }}, {{ format_json_path(normalized_json_path) }})
{%- endmacro %}
