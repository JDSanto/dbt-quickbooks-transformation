{% macro last_day(date, datepart) %}
    cast(
        {{dateadd('day', '-1',
        dateadd(datepart, '1', date_trunc(datepart, date))
        )}}
        as date)
{% endmacro %}
