{% macro dateadd(datepart, interval, from_date_or_timestamp) %}
    date_add(
        {{ from_date_or_timestamp }}, interval 1 {{ datepart }}
    )
{% endmacro %}
