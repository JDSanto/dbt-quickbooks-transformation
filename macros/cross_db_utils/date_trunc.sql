{% macro date_trunc(datepart, date) -%}    
    {% if datepart == 'month' %}
        date( {{date}} + INTERVAL (1 - day( {{date}} )) DAY)

    {% elif datepart == 'year' %}
        date(makedate(year( {{ date }} ), 1))

    {% else %}
        date_trunc('{{datepart}}', {{date}})

    {% endif %}
{% endmacro %}
