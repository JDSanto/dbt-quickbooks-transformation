{{ config(schema="analytics", tags=["top-level"]) }}
with final as (
    select
        {{ common_fields() }},
        {{ json_extract_scalar('_airbyte_data', ['FullyQualifiedName']) }} fully_qualified_name,
        cast({{ json_extract_scalar('_airbyte_data', ['Active']) }} as {{ dbt_utils.type_int() }} ) is_active,
        {{ json_extract_scalar('_airbyte_data', ['Name']) }} name,
        {{ json_extract_scalar('_airbyte_data', ['AcctNum']) }} as account_number,
        cast({{ json_extract_scalar('_airbyte_data', ['SubAccount']) }} as {{ dbt_utils.type_int() }}) as is_sub_account,
        {{ json_extract_scalar('_airbyte_data', ['ParentRef', 'value']) }} as parent_account_id,
        {{ json_extract_scalar('_airbyte_data', ['AccountType']) }} account_type,
        {{ json_extract_scalar('_airbyte_data', ['AccountSubType']) }} account_sub_type,
        {{ json_extract_scalar('_airbyte_data', ['Classification']) }} classification,
        cast({{ json_extract_scalar('_airbyte_data', ['CurrentBalance']) }} as {{ dbt_utils.type_numeric() }}) balance,
        cast({{ json_extract_scalar('_airbyte_data', ['CurrentBalanceWithSubAccounts']) }} as {{ dbt_utils.type_numeric() }}) balance_with_sub_accounts,
        {{ json_extract_scalar('_airbyte_data', ['CurrencyRef', 'name']) }} currency_name,
        {{ json_extract_scalar('_airbyte_data', ['Description']) }} description
    from
        {{ source('analytics', '_airbyte_raw_quickbook_accounts') }}
)
select
    *,
    {{ hash_field() }}
from
    final
where
    {{ incremental_filter() }}
