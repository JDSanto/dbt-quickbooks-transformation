{{ config(schema="analytics", tags=["top-level"]) }}
{{ unnest_cte('quickbook_bills', 'quickbook_bills', 'line') }}
select
    _hash_id as _bill_hash_id,
    id as bill_id,
    {{ json_extract_scalar(unnested_column_value('line'), ['Id']) }} as id,
    {{ json_extract_scalar(unnested_column_value('line'), ['AccountBasedExpenseLineDetail', 'AccountRef', 'value']) }} as account_expense_account_id,
    {{ json_extract_scalar(unnested_column_value('line'), ['AccountBasedExpenseLineDetail', 'ClassRef', 'value']) }} as account_expense_class_id,
    {{ json_extract_scalar(unnested_column_value('line'), ['AccountBasedExpenseLineDetail', 'CustomerRef', 'value']) }} as account_expense_customer_id,
    {{ json_extract_scalar(unnested_column_value('line'), ['AccountBasedExpenseLineDetail', 'BillableStatus']) }} as account_expense_billable_status,
    {{ json_extract_scalar(unnested_column_value('line'), ['ItemBasedExpenseLineDetail', 'ClassRef', 'value']) }} as item_expense_class_id,
    {{ json_extract_scalar(unnested_column_value('line'), ['ItemBasedExpenseLineDetail', 'CustomerRef', 'value']) }} as item_expense_customer_id,
    {{ json_extract_scalar(unnested_column_value('line'), ['ItemBasedExpenseLineDetail', 'BillableStatus']) }} as item_expense_billable_status,
    {{ json_extract_scalar(unnested_column_value('line'), ['ItemBasedExpenseLineDetail', 'ItemRef', 'value']) }} as item_expense_item_id,
    {{ json_extract_scalar(unnested_column_value('line'), ['Description']) }} as description,
    cast({{ json_extract_scalar(unnested_column_value('line'), ['Amount']) }} as {{ dbt_utils.type_numeric() }}) as amount
from {{ ref('quickbook_bills') }}
{{ cross_join_unnest('quickbook_bills', 'line') }}
where
    line is not null
    and {{ incremental_filter_line(ref('quickbook_bills'))}}