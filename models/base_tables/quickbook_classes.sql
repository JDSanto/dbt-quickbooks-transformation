{{ config(schema="analytics", tags=["top-level"]) }}
with final as (
    select
        {{ common_fields() }},
        {{ json_extract_scalar('_airbyte_data', ['FullyQualifiedName']) }} as fully_qualified_name,
        cast({{ json_extract_scalar('_airbyte_data', ['Active']) }} as {{ dbt_utils.type_int() }} ) as is_active,
        {{ json_extract_scalar('_airbyte_data', ['Name']) }} as name,
        cast({{ json_extract_scalar('_airbyte_data', ['SubClass']) }} as {{ dbt_utils.type_int() }} ) as is_subclass
    from
        {{ source('analytics', '_airbyte_raw_quickbook_classes') }}
)
select
    *,
    {{ hash_field() }}
from
    final
where
    {{ incremental_filter() }}