{{ config(schema="analytics", tags=["top-level"]) }}
{{ unnest_cte('quickbook_bill_payments', 'quickbook_bill_payments', 'line') }},
bill_payment as (
    select
        _hash_id as _bill_payment_hash_id,
        id as bill_payment_id,
        json_value({{unnested_column_value('line')}}, '$."LinkedTxn"[0]."TxnId"') as transaction_id,
        json_value({{unnested_column_value('line')}}, '$."LinkedTxn"[0]"."TxnType"') as transaction_type,
        cast({{ json_extract_scalar(unnested_column_value('line'), ['Amount']) }} as {{ dbt_utils.type_numeric() }}) as amount
    from {{ ref('quickbook_bill_payments') }}
    {{ cross_join_unnest('quickbook_bill_payments', 'line') }}
    where 
        line is not null
        and {{ incremental_filter_line(ref('quickbook_bill_payments'))}}
)
select
    *,
    if(transaction_type = 'Bill', transaction_id, null) as bill_id,
    if(transaction_type = 'Deposit', transaction_id, null) as deposit_id,
    if(transaction_type = 'JournalEntry', transaction_id, null) as journal_entry_id,
    if(transaction_type = 'Expense', transaction_id, null) as expense_id
from bill_payment
