{{ config(schema="analytics", tags=["top-level"]) }}
with final as (
    select
        {{ common_fields() }},
        {{ json_extract_scalar('_airbyte_data', ['ARAccountRef', 'value']) }} as receivable_account_id,
        {{ json_extract_scalar('_airbyte_data', ['DepositToAccountRef', 'value']) }} as deposit_to_account_id,
        cast({{ json_extract_scalar('_airbyte_data', ['UnappliedAmt']) }} as {{ dbt_utils.type_numeric() }}) as unapplied_amount,
        {{ json_extract_scalar('_airbyte_data', ['CurrencyRef', 'name']) }} as currency_name,
        cast({{ json_extract_scalar('_airbyte_data', ['TotalAmt']) }} as {{ dbt_utils.type_numeric() }}) as total_amount,
        {{ string_to_timestamp(json_extract_scalar('_airbyte_data', ['TxnDate'])) }} as transaction_date,
        {{ json_extract_scalar('_airbyte_data', ['CustomerRef', 'value']) }} as customer_id,
        {{ json_extract('_airbyte_data', ['Line']) }} as line
    from
        {{ source('analytics', '_airbyte_raw_quickbook_payments') }}
)
select
    *,
    {{ hash_field() }}
from
    final
where
    {{ incremental_filter() }}
