{{ config(schema="analytics", tags=["top-level"]) }}
with final as (
    select
        {{ common_fields() }},
        {{ json_extract_scalar('_airbyte_data', ['AccountRef', 'value']) }} as account_id,
        {{ json_extract_scalar('_airbyte_data', ['DocNumber']) }} as doc_number,
        {{ json_extract_scalar('_airbyte_data', ['Credit']) }} as credit,
        {{ json_extract_scalar('_airbyte_data', ['CurrencyRef', 'name']) }} as currency_name,
        cast({{ json_extract_scalar('_airbyte_data', ['TotalAmt']) }} as {{ dbt_utils.type_numeric() }}) as total_amount,
        {{ json_extract_scalar('_airbyte_data', ['PaymentType']) }} as payment_type,
        {{ string_to_timestamp(json_extract_scalar('_airbyte_data', ['TxnDate'])) }} as transaction_date,
        {{ json_extract_scalar('_airbyte_data', ['EntityRef', 'value']) }} as entity_id,
        {{ json_extract_scalar('_airbyte_data', ['EntityRef', 'type']) }} as entity_type,
        {{ json_extract('_airbyte_data', ['Line']) }} as line
    from
        {{ source('analytics', '_airbyte_raw_quickbook_purchases') }}
)
select
    *,
    if(entity_type = 'Customer', entity_id, null) as customer_id,
    {{ hash_field() }}
from
    final
where
    {{ incremental_filter() }}
