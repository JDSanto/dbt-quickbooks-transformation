{{ config(schema="analytics", tags=["top-level"]) }}
with final as (
    select
        {{ common_fields() }},
        {{ json_extract_scalar('_airbyte_data', ['CheckPayment', 'BankAccountRef', 'value']) }} as check_bank_account_id,
        {{ json_extract_scalar('_airbyte_data', ['CheckPayment', 'PrintStatus']) }} as check_print_status,
        {{ json_extract_scalar('_airbyte_data', ['CreditCardPayment', 'CCAccountRef', 'value']) }} as credit_card_account_id,
        {{ json_extract_scalar('_airbyte_data', ['CurrencyRef', 'name']) }} as currency_name,
        {{ json_extract_scalar('_airbyte_data', ['PayType']) }} as pay_type,
        cast({{ json_extract_scalar('_airbyte_data', ['TotalAmt']) }} as {{ dbt_utils.type_numeric() }}) as total_amount,
        {{ string_to_timestamp(json_extract_scalar('_airbyte_data', ['TxnDate'])) }} as transaction_date,
        {{ json_extract('_airbyte_data', ['Line']) }} as line
    from
        {{ source('analytics', '_airbyte_raw_quickbook_bill_payments') }}
)
select
    *,
    {{ hash_field() }}
from
    final
where
    {{ incremental_filter() }}