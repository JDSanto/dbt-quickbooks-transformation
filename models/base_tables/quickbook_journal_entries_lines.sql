{{ config(schema="analytics", tags=["top-level"]) }}

{{ unnest_cte('quickbook_journal_entries', 'quickbook_journal_entries', 'line') }}
select
    _hash_id as _journal_entry_hash_id,
    id as journal_entry_id,
    {{ json_extract_scalar(unnested_column_value('line'), ['Id']) }} as id,
    cast({{ json_extract_scalar(unnested_column_value('line'), ['Amount']) }} as {{ dbt_utils.type_numeric() }}) as amount,
    {{ json_extract_scalar(unnested_column_value('line'), ['JournalEntryLineDetail', 'AccountRef', 'value']) }} as account_id,
    {{ json_extract_scalar(unnested_column_value('line'), ['JournalEntryLineDetail', 'ClassRef', 'value']) }} as class_id,
    {{ json_extract_scalar(unnested_column_value('line'), ['JournalEntryLineDetail', 'CustomerRef', 'value']) }} as customer_id,
    {{ json_extract_scalar(unnested_column_value('line'), ['JournalEntryLineDetail', 'PostingType']) }} as posting_type,
    {{ json_extract_scalar(unnested_column_value('line'), ['Description']) }} as description
from {{ ref('quickbook_journal_entries') }}
{{ cross_join_unnest('quickbook_journal_entries', 'line') }}
where
    line is not null
    and {{ incremental_filter_line(ref('quickbook_journal_entries'))}}
