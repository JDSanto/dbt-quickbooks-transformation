{{ config(schema="analytics", tags=["top-level"]) }}
with final as (
    select
        {{ common_fields() }},
        {{ json_extract_scalar('_airbyte_data', ['FullyQualifiedName']) }} as fully_qualified_name,
        cast({{ json_extract_scalar('_airbyte_data', ['Active']) }} as {{ dbt_utils.type_int() }} ) as is_active,
        cast({{ json_extract_scalar('_airbyte_data', ['Balance']) }} as {{ dbt_utils.type_numeric() }}) as balance,
        cast({{ json_extract_scalar('_airbyte_data', ['BalanceWithJobs']) }} as {{ dbt_utils.type_numeric() }}) as balance_with_jobs,
        cast({{ json_extract_scalar('_airbyte_data', ['BillWithParent']) }} as {{ dbt_utils.type_int() }} ) as bill_with_parent,
        {{ json_extract_scalar('_airbyte_data', ['CompanyName']) }} as company_name,
        {{ json_extract_scalar('_airbyte_data', ['CurrencyRef', 'name']) }} as currency_name,
        {{ json_extract_scalar('_airbyte_data', ['DisplayName']) }} as display_name,
        {{ json_extract_scalar('_airbyte_data', ['WebAddr', 'URI']) }} as website,
        cast({{ json_extract_scalar('_airbyte_data', ['Taxable']) }} as {{ dbt_utils.type_numeric() }}) as taxable
    from
        {{ source('analytics', '_airbyte_raw_quickbook_customers') }}
)
select
    *,
    {{ hash_field() }}
from
    final
where
    {{ incremental_filter() }}