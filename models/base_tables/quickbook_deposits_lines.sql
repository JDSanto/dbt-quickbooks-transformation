{{ config(schema="analytics", tags=["top-level"]) }}
{{ unnest_cte('quickbook_deposits', 'quickbook_deposits', 'line') }}
select
    _hash_id as _deposit_hash_id,
    id as deposit_id,
    {{ json_extract_scalar(unnested_column_value('line'), ['Id']) }} as id,
    cast({{ json_extract_scalar(unnested_column_value('line'), ['Amount']) }} as {{ dbt_utils.type_numeric() }}) as amount,
    {{ json_extract_scalar(unnested_column_value('line'), ['DepositLineDetail', 'AccountRef', 'value']) }} as account_id,
    {{ json_extract_scalar(unnested_column_value('line'), ['DepositLineDetail', 'ClassRef', 'value']) }} as class_id,
    {{ json_extract_scalar(unnested_column_value('line'), ['DepositLineDetail', 'CustomerRef', 'value']) }} as customer_id,
    {{ json_extract_scalar(unnested_column_value('line'), ['LineNum']) }} as line_num
from {{ ref('quickbook_deposits') }}
    {{ cross_join_unnest('quickbook_deposits', 'line') }}
where
    line is not null
    and {{ incremental_filter_line(ref('quickbook_deposits'))}}
