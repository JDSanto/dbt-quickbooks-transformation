{{ config(schema="analytics", tags=["top-level"]) }}
{{ unnest_cte('quickbook_invoices', 'quickbook_invoices', 'line') }}
select
    _hash_id as _invoice_hash_id,
    id as invoice_id,
    {{ json_extract_scalar(unnested_column_value('line'), ['Id']) }} as id,
    cast({{ json_extract_scalar(unnested_column_value('line'), ['Amount']) }} as {{ dbt_utils.type_numeric() }}) as amount,
    {{ json_extract_scalar(unnested_column_value('line'), ['SalesItemLineDetail', 'ItemAccountRef', 'value']) }} as sales_item_account_id,
    {{ json_extract_scalar(unnested_column_value('line'), ['SalesItemLineDetail', 'ItemRef', 'value']) }} as sales_item_item_id,
    {{ json_extract_scalar(unnested_column_value('line'), ['SalesItemLineDetail', 'ClassRef', 'value']) }} as sales_item_class_id,
    {{ json_extract_scalar(unnested_column_value('line'), ['SalesItemLineDetail', 'Qty']) }} as sales_item_quantity,
    {{ json_extract_scalar(unnested_column_value('line'), ['SalesItemLineDetail', 'UnitPrice']) }} as sales_item_unit_price,
    {{ json_extract_scalar(unnested_column_value('line'), ['Description']) }} as description,
    {{ json_extract_scalar(unnested_column_value('line'), ['LineNum']) }} as line_num
from {{ ref('quickbook_invoices') }}
{{ cross_join_unnest('quickbook_invoices', 'line') }}
where
    line is not null
    and {{ incremental_filter_line(ref('quickbook_invoices'))}}
