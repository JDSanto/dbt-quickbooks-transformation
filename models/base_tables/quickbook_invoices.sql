{{ config(schema="analytics", tags=["top-level"]) }}
with final as (
    select
        {{ common_fields() }},
        {{ json_extract_scalar('_airbyte_data', ['DepositToAccountRef', 'value']) }} as account_id,
        {{ json_extract_scalar('_airbyte_data', ['CustomerRef', 'value']) }} as customer_id,
        cast({{ json_extract_scalar('_airbyte_data', ['Balance']) }} as {{ dbt_utils.type_numeric() }}) as balace,
        {{ json_extract_scalar('_airbyte_data', ['DocNumber']) }} as doc_number,
        {{ json_extract_scalar('_airbyte_data', ['CurrencyRef', 'name']) }} as currency_name,
        cast({{ json_extract_scalar('_airbyte_data', ['TotalAmt']) }} as {{ dbt_utils.type_numeric() }}) as total_amount,
        {{ string_to_timestamp(json_extract_scalar('_airbyte_data', ['TxnDate'])) }} as transaction_date,
        {{ string_to_timestamp(json_extract_scalar('_airbyte_data', ['DueDate'])) }} as due_date,
        {{ json_extract('_airbyte_data', ['Line']) }} as line
    from
        {{ source('analytics', '_airbyte_raw_quickbook_invoices') }}
)
select
    *,
    {{ hash_field() }}
from
    final
where
    {{ incremental_filter() }}