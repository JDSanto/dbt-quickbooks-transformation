{{ config(schema="analytics", tags=["top-level"]) }}
with final as (
    select
        {{ common_fields() }},
        cast({{ json_extract_scalar('_airbyte_data', ['Active']) }} as {{ dbt_utils.type_int() }} ) as is_active,
        {{ json_extract_scalar('_airbyte_data', ['AssetAccountRef', 'value']) }} as asset_account_id,
        {{ json_extract_scalar('_airbyte_data', ['ExpenseAccountRef', 'value']) }} as expense_account_id,
        {{ json_extract_scalar('_airbyte_data', ['Description']) }} as description,
        {{ json_extract_scalar('_airbyte_data', ['FullyQualifiedName']) }} as fully_qualified_name,
        {{ json_extract_scalar('_airbyte_data', ['IncomeAccountRef', 'value']) }} as income_account_id,
        {{ json_extract_scalar('_airbyte_data', ['Name']) }} as name,
        {{ json_extract_scalar('_airbyte_data', ['ParentRef', 'value']) }} as parent_item_id,
        cast({{ json_extract_scalar('_airbyte_data', ['PurchaseCost']) }} as {{ dbt_utils.type_numeric() }}) as purchase_cost,
        {{ json_extract_scalar('_airbyte_data', ['PurchaseDesc']) }} as purchase_description,
        cast({{ json_extract_scalar('_airbyte_data', ['SubItem']) }} as {{ dbt_utils.type_int() }} ) as sub_item,
        cast({{ json_extract_scalar('_airbyte_data', ['Taxable']) }} as {{ dbt_utils.type_int() }} ) as taxable,
        cast({{ json_extract_scalar('_airbyte_data', ['TrackQtyOnHand']) }} as {{ dbt_utils.type_int() }} ) as track_quantity_on_hand,
        {{ json_extract_scalar('_airbyte_data', ['Type']) }} as type,
        cast({{ json_extract_scalar('_airbyte_data', ['UnitPrice']) }} as {{ dbt_utils.type_int() }} ) as unit_price
    from
        {{ source('analytics', '_airbyte_raw_quickbook_items') }}
)
select
    *,
    {{ hash_field() }}
from
    final
where
    {{ incremental_filter() }}