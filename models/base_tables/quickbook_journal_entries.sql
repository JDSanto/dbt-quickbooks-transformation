{{ config(schema="analytics", tags=["top-level"]) }}
with final as (
    select
        {{ common_fields() }},
        cast({{ json_extract_scalar('_airbyte_data', ['Adjustment']) }} as {{ dbt_utils.type_int() }} ) as is_adjustment,
        {{ json_extract_scalar('_airbyte_data', ['CurrencyRef', 'name']) }} as currency_name,
        {{ json_extract('_airbyte_data', ['Line']) }} as line,
        {{ json_extract_scalar('_airbyte_data', ['PrivateNote']) }} as private_note,
        {{ string_to_timestamp(json_extract_scalar('_airbyte_data', ['TxnDate'])) }} as transaction_date
    from
        {{ source('analytics', '_airbyte_raw_quickbook_journal_entries') }}
)
select
    *,
    {{ hash_field() }}
from
    final
where
    {{ incremental_filter() }}