{{ config(schema="analytics", tags=["top-level"]) }}

with gl_union as (
    select
        transaction_id,
        transaction_date,
        -- cast(null as int) as vendor_id,
        amount,
        account_id,
        transaction_type,
        transaction_source,
        currency_name,
        class_id,
        customer_id
    from {{ref('int_quickbooks__purchase_double_entry')}}

    union all

    select *
    from {{ref('int_quickbooks__deposit_double_entry')}}

    union all

    select *
    from {{ref('int_quickbooks__journal_entry_double_entry')}}

    union all

    select *
    from {{ref('int_quickbooks__payment_double_entry')}}

    union all

    select *
    from {{ref('int_quickbooks__bill_double_entry')}}

    union all

    select *
    from {{ref('int_quickbooks__invoice_double_entry')}}

    union all

    select *
    from {{ref('int_quickbooks__bill_payment_double_entry')}}

    union all

    select *
    from {{ref('int_quickbooks__payroll_check_double_entry')}}
),

accounts as (
    select *
    from {{ref('int_quickbooks__account_classifications')}}
),


adjusted_gl as (
    select
        gl_union.transaction_id,
        row_number() over(partition by gl_union.transaction_id order by gl_union.transaction_date) as transaction_index,
        gl_union.transaction_date,
        gl_union.amount,
        gl_union.account_id,
        -- accounts.name as account_name,
        -- accounts.is_sub_account,
        -- accounts.account_type,
        -- accounts.account_sub_type,
        accounts.financial_statement_helper,
        -- accounts.balance as account_current_balance,
        accounts.classification as account_classification, 
        gl_union.transaction_type,
        gl_union.transaction_source,
        gl_union.currency_name,
        gl_union.class_id,
        gl_union.customer_id,
        accounts.transaction_type as account_transaction_type,
        case when accounts.transaction_type = gl_union.transaction_type
            then gl_union.amount
            else gl_union.amount * -1
                end as adjusted_amount
    from gl_union

    left join accounts
        on gl_union.account_id = accounts.id
),

final as (
    select
        *,
        sum(adjusted_amount) over (partition by account_id order by transaction_date, account_id rows unbounded preceding) as running_balance
    from adjusted_gl
)

select *
from final