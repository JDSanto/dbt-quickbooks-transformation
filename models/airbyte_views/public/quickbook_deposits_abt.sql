{{ config(enabled=False) }}

select
    qd.id as deposit_id,
    qd.account_id as deposit_account_id,
    qd.created_at as deposit_created_at,
    qd.currency_name as deposit_currency_name,
    qd.line as deposit_line,
    qd.total_amount as deposit_total_amount,
    qd.transaction_date as deposit_transaction_date,
    qd.updated_at as deposit_updated_at,
    qdl.id as deposit_line_id,
    qdl.amount as deposit_line_amount,
    qdl.line_num as deposit_line_num,
    qdl.class_id as deposit_line_class_id,
    qdl.account_id as deposit_line_account_id,
    qdl.customer_id as deposit_line_customer_id,
    qad.fully_qualified_name as deposit_account_fully_qualified_name,
    qad.name as deposit_account_name,
    qad.account_number as deposit_account_number,
    qad.parent_account_id as deposit_account_parent_account_id,
    qad.account_type as deposit_account_type,
    qad.account_sub_type as deposit_account_sub_type,
    qad.classification as deposit_account_classification,
    qadl.fully_qualified_name as deposit_line_account_fully_qualified_name,
    qadl.name as deposit_line_account_name,
    qadl.account_number as deposit_line_account_number,
    qadl.parent_account_id as deposit_line_account_parent_account_id,
    qadl.account_type as deposit_line_account_type,
    qadl.account_sub_type as deposit_line_account_sub_type,
    qadl.classification as deposit_line_account_classification,
    qc.fully_qualified_name as deposit_line_customer_fully_qualified_name,
    qc.company_name as deposit_line_customer_company_name,
    qc.display_name as deposit_line_customer_display_name,
    qcl.fully_qualified_name as deposit_line_class_fully_qualified_name,
    qd._airbyte_quickbook_deposits_hashid as _airbyte_quickbook_deposits_hashid,
    qd._airbyte_emitted_at as _airbyte_emitted_at
from
    {{ ref('quickbook_deposits') }} qd
left join {{ ref('quickbook_deposits_lines') }} qdl using (`_airbyte_quickbook_deposits_hashid`)
left join {{ ref('quickbook_accounts') }} qadl on (qadl.id = qdl.account_id)
left join {{ ref('quickbook_accounts') }} qad on (qad.id = qd.account_id)
left join {{ ref('quickbook_customers') }} qc on (qc.id = qdl.customer_id)
left join {{ ref('quickbook_classes') }} qcl on (qcl.id = qdl.class_id)