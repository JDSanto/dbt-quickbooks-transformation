{{ config(enabled=False) }}

select
    qje.id as journal_entry_id,
    qje.created_at as journal_entry_created_at,
    qje.currency_name as journal_entry_currency_name,
    qje.line as journal_entry_line,
    qje.private_note as journal_entry_private_note,
    qje.is_adjustment as journal_entry_is_adjustment,
    qje.transaction_date as journal_entry_transaction_date,
    qje.updated_at as journal_entry_updated_at,
    qjel.id as journal_entry_line_id,
    qjel.amount as journal_entry_line_amount,
    qjel.description as journal_entry_line_description,
    qjel.class_id as journal_entry_line_class_id,
    qjel.account_id as journal_entry_line_account_id,
    qjel.customer_id as journal_entry_line_customer_id,
    qjel.posting_type as journal_entry_line_posting_type,
    qajel.fully_qualified_name as journal_entry_line_account_fully_qualified_name,
    qajel.name as journal_entry_line_account_name,
    qajel.account_number as journal_entry_line_account_number,
    qajel.parent_account_id as journal_entry_line_account_parent_account_id,
    qajel.account_type as journal_entry_line_account_type,
    qajel.account_sub_type as journal_entry_line_account_sub_type,
    qajel.classification as journal_entry_line_account_classification,
    qc.fully_qualified_name as journal_entry_line_customer_fully_qualified_name,
    qc.company_name as journal_entry_line_customer_company_name,
    qc.display_name as journal_entry_line_customer_display_name,
    qcl.fully_qualified_name as journal_entry_line_class_fully_qualified_name,
    qje._airbyte_quickbook_journal_entries_hashid as _airbyte_quickbook_journal_entries_hashid,
    qje._airbyte_emitted_at as _airbyte_emitted_at
from
    {{ ref('quickbook_journal_entries') }} qje
left join {{ ref('quickbook_journal_entries_lines') }} qjel using (`_airbyte_quickbook_journal_entries_hashid`)
left join {{ ref('quickbook_accounts') }} qajel on (qajel.id = qjel.account_id)
left join {{ ref('quickbook_customers') }} qc on (qc.id = qjel.customer_id)
left join {{ ref('quickbook_classes') }} qcl on (qcl.id = qjel.class_id)


-- WITH RECURSIVE tree AS ( 
--    SELECT id, 
--           parent_account_id,
--           1 as level,
--           id AS top_level_id
--    FROM quickbook_accounts
--    WHERE parent_account_id IS NULL

--    UNION ALL

--    SELECT c.id,
--           c.parent_account_id, 
--           t.level + 1,
--           COALESCE(t.top_level_id) AS top_level_id
--    FROM quickbook_accounts c
--      JOIN tree t ON c.parent_account_id = t.id
-- )
-- SELECT *
-- FROM tree;