{{ config(enabled=False) }}

select
    qp.id as purchase_id,
    qp.account_id as purchase_account_id,
    qp.created_at as purchase_created_at,
    qp.currency_name as purchase_currency_name,
    qp.line as purchase_line,
    qp.total_amount as purchase_total_amount,
    qp.transaction_date as purchase_transaction_date,
    qp.updated_at as purchase_updated_at,
    qpl.id as purchase_line_id,
    qpl.amount as purchase_line_amount,
    qpl.description as purchase_line_description,
    qpl.account_expense_account_id as purchase_line_account_expense_account_id,
    qpl.account_expense_class_id as purchase_line_account_expense_class_id,
    qpl.account_expense_customer_id as purchase_line_account_expense_customer_id,
    qpl.item_expense_item_id as purchase_line_item_expense_item_id,
    qpl.item_expense_billable_status as purchase_line_item_expense_billable_status,
    qap.fully_qualified_name as purchase_account_fully_qualified_name,
    qap.name as purchase_account_name,
    qap.parent_account_id as purchase_account_parent_account_id,
    qap.account_number as purchase_account_number,
    qap.account_type as purchase_account_type,
    qap.account_sub_type as purchase_account_sub_type,
    qap.classification as purchase_account_classification,
    qapl.fully_qualified_name as purchase_line_account_fully_qualified_name,
    qapl.name as purchase_line_account_name,
    qapl.parent_account_id as purchase_line_account_parent_account_id,
    qapl.account_number as purchase_line_account_number,
    qapl.account_type as purchase_line_account_type,
    qapl.account_sub_type as purchase_line_account_sub_type,
    qapl.classification as purchase_line_account_classification,
    qc.fully_qualified_name as purchase_line_customer_fully_qualified_name,
    qc.company_name as purchase_line_customer_company_name,
    qc.display_name as purchase_line_customer_display_name,
    qcl.fully_qualified_name as purchase_line_class_fully_qualified_name,
    qp._airbyte_quickbook_purchases_hashid as _airbyte_quickbook_purchases_hashid,
    qp._airbyte_emitted_at as _airbyte_emitted_at
from
    {{ ref('quickbook_purchases') }} qp
left join {{ ref('quickbook_purchases_lines') }} qpl using (`_airbyte_quickbook_purchases_hashid`)
left join {{ ref('quickbook_accounts') }} qapl on (qapl.id = qpl.account_expense_account_id)
left join {{ ref('quickbook_accounts') }} qap on (qap.id = qp.account_id)
left join {{ ref('quickbook_customers') }} qc on (qc.id = qpl.account_expense_customer_id)
left join {{ ref('quickbook_classes') }} qcl on (qcl.id = qpl.account_expense_class_id)